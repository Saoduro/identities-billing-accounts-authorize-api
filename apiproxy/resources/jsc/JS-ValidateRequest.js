 var billingId = context.getVariable("request.queryparam.billingId");
 var lastName = context.getVariable("request.queryparam.lastName");
 var postalCode = context.getVariable("request.queryparam.postalCode");
 
 // validate billingId
 var regex = new RegExp(/^[0-9]{4,}$/);
 if(billingId === null){
     context.setVariable("isRequestValid","false");
     context.setVariable("queryParam", "billingId");
     context.setVariable("errorMessage", "Please provide a valid billingId");
 } else if(!regex.test(billingId)) {
     context.setVariable("isRequestValid","false");
     context.setVariable("queryParam", "billingId");
     context.setVariable("errorMessage", "String is too short (" + billingId.length + " chars), minimum 4");
 }
 // validate lastName
 if(lastName === null || lastName.trim() === "") {
     context.setVariable("isRequestValid","false");
     context.setVariable("queryParam", "lastName");
     context.setVariable("errorMessage", "Please provide a valid lastName");
 
 }
 
 // validate postalCode
 var postalCodeRegex = new RegExp(/^\d{5}(?:[-\s]\d{4})?$/);
 if(!postalCodeRegex.test(postalCode)) {
      context.setVariable("isRequestValid","false");
     context.setVariable("queryParam", "postalCode");
     context.setVariable("errorMessage", "Please provide a valid postalCode");
 }
 
// timestamp
var timeStamp = new Date().toISOString().replace('Z', '');
context.setVariable("timeStamp", timeStamp);

